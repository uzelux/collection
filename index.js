/**
 * Collection Class, an Array of Objects
 */
class Collection {
  /**
   * Create a Collection
   * @param   {Object|Object[]|Array} inputs
   * @return  {Collection}
   */
  constructor(...inputs) {
    this.content = (inputs.length === 1 && inputs[0].constructor.name === 'Array') ? [...inputs[0]] : [...inputs];

    return new Proxy(this, {
      get(target, prop) {
        if (prop.constructor.name === 'Symbol') {
          return target[prop];
        }
        if (Number.isInteger(Number.parseInt(prop))) {
          return target.content[prop];
        }
        return target[prop];
      },
    });
  }

  /**
   * Length of this collection
   * @return {number}
   */
  get length() {return this.content.length;}

  /**
   * First Element of this collection
   * @return {*}
   */
  get first() {return this.content[0];}

  /**
   * Last Element of this collection
   * @return {*}
   */
  get last() {return this.content[this.content.length - 1];}

  [Symbol.iterator]() {
    let index = -1;
    const output = [...this.content];
    return {
      next() {
        index++;
        return { value: output[index], done: (index >= output.length) }
      },
    }
  }
}

// Array Functions

/**
 * Create a new Collection by concatenating additional Collections or Arrays to the Base Collection
 * @see Array#concat
 * @param   {Collection}             collection  - Base Collection
 * @param   {...(Collection|Array)}  inputs      - additional collections or arrays
 * @return  {Collection}                         - a new Collection
 */
Collection.concat = (collection, ...inputs) => {
  if (collection.constructor.name !== 'Collection') throw TypeError(`Base should be a Collection, got ${collection.constructor.name} instead`);

  return new Collection(inputs.reduce((output, input) => {
    if (input.constructor.name !== 'Collection' || input.constructor.name !== 'Array') {
      throw TypeError(`Inputs should be Collections or Arrays, got ${input.constructor.name} instead`);
    }
    return [...output, ...input];
  }, [...collection]));
};

/**
 * Concatenate additional Collections or Arrays to this collection
 * @see Array#concat
 * @param   {...(Collection|Array)}  inputs - additional collections or arrays
 * @return  {Collection}                    - this collection
 */
Collection.prototype.concat = function (...inputs) {
  this.content = inputs.reduce((output, input) => {
    if (input.constructor.name !== 'Collection' || input.constructor.name !== 'Array') {
      throw TypeError(`Inputs should be Collections or Arrays, got ${input.constructor.name} instead`);
    }
    return [...output, ...input];
  }, [...this.content]);
  return this;
};

/**
 * Create a new Collection with an additional element appended to the end of the Base Collection
 * @see Array#push
 * @param   {Collection}  collection  - Base Collection
 * @param   {Object}      input       - new element
 * @return  {Collection}              - a new Collection
 */
Collection.push = (collection, input) => new Collection([...collection, input]);

/**
 * Append an new element to the end of this collection
 * @see Array#push
 * @param   {Object} input  - new element
 * @return  {Collection}    - this collection
 */
Collection.prototype.push = function (input) {
  this.content.push(input);
  return this;
};

/**
 * Create a new Collection with the last element removed from the Base Collection
 * @see Array#pop
 * @param   {Collection}  collection  - Base Collection
 * @return  {Collection}              - a new Collection
 */
Collection.pop = (collection) => new Collection([...collection].slice(0, -1));

/**
 * Remove the last element from this collection
 * @see Array#pop
 * @return  {Collection}- this collection
 */
Collection.prototype.pop = function () {
  this.content.pop();
  return this;
};

/**
 * Create a new Collection with the first element removed from the Base Collection
 * @see Array#shift
 * @param   {Collection}  collection  - Base Collection
 * @return  {Collection}              - a new Collection
 */
Collection.shift = (collection) => new Collection([...collection].slice(1));

/**
 * Remove  the first element from this collection
 * @see Array#shift
 * @return  {Collection}              - this
 */
Collection.prototype.shift = function () {
  this.content.shift();
  return this;
};

/**
 * Create a new Collection with a slice of the Base Collection
 * @see Array#slice
 * @param   {Collection}  collection  - Base Collection
 * @param   {number}      [begin]     - start index
 * @param   {number}      [end]       - end index
 * @return  {Collection}              - a new Collection
 */
Collection.slice = (collection, begin, end) => new Collection([...collection].slice(begin, end));

/**
 * Slice off values and keep a subset of this collection
 * @see Array#slice
 * @param   {number}  [begin] - start index
 * @param   {number}  [end]   - end index
 * @return  {Collection}      - this collection
 */
Collection.prototype.slice = function (begin, end) {
  this.content = this.content.slice(begin, end);
  return this;
};

/**
 * Create a new Collection with a modified value set from the Base Collection
 * @see Array#splice
 * @param   {Collection}  collection    - Base Collection
 * @param   {number}      start         - start index
 * @param   {number}      [deleteCount] - amount of entries to be deleted, starting from start index
 * @param   {...Object}   [inputs]      - new elements
 * @return  {Collection}                - a new Collection
 */
Collection.splice = (collection, start, deleteCount, ...inputs) => new Collection([...collection].splice(
  start,
  deleteCount,
  ...inputs,
));

/**
 * Modify the value set of this collection
 * @see Array#splice
 * @param   {number}    start         - start index
 * @param   {number}    [deleteCount] - amount of entries to be deleted, starting from start index
 * @param   {...Object} [inputs]      - new elements
 * @return  {Collection}              - this collection
 */
Collection.prototype.splice = function (start, deleteCount, ...inputs) {
  this.content.splice(start, deleteCount, ...inputs);
  return this;
};

/**
 * Create a new Collection with new elements added to the front of the Base Collection value set
 * @see Array#unshift
 * @param   {Collection}  collection  - Base Collection
 * @param   {...Object}   inputs      - new elements
 * @return  {Collection}              - a new Collection
 */
Collection.unshift = (collection, ...inputs) => new Collection([...collection].unshift(...inputs));

/**
 * Add new elements to the front of this collection
 * @see Array#unshift
 * @param   {...Object}   input - new elements
 * @return  {Collection}        - this collection
 */
Collection.prototype.unshift = function (...input) {
  this.content.unshift(...input);
  return this;
};

/**
 * Returns a new Iterator object that contains the values of this collection
 * @see Array#values
 * @return {Iterator}
 */
Collection.prototype.values = function () {
  return this.content.values();
};

/**
 * Returns the underlying array of this collection
 * @return {Array}
 */
Collection.prototype.toArray = function () {
  return this.content;
};

/**
 * Returns the underlying array of this collection
 * @alias Collection#toArray
 * @return {Array}
 */
Collection.prototype.get = function () {
  return this.content;
};

/**
 * Returns the underlying array of this collection
 * @alias Collection#toArray
 * @return {Array}
 */
Collection.prototype.fetch = function () {
  return this.content;
};

/**
 * Returns a string representation of this collection
 * @return {string}
 */
Collection.prototype.toString = function () {
  return JSON.stringify(this.content);
};

// Collection Filter Functions

/**
 * Create a new Collection with the filtered result of the Base Collection
 * @see Array#filter
 * @param   {Collection}  collection  - Base Collection
 * @param   {Function}    callback    - the filtering callback to apply to the Base Collection
 * @return {Collection}               - a new Collection
 */
Collection.filter = (collection, callback) => new Collection(collection.content.filter(callback));

/**
 * Filter out elements from this collection
 * @see Array#filter
 * @param {Function}    callback  - the filtering callback
 * @return {Collection}           - this collection
 */
Collection.prototype.filter = function (callback) {
  this.content = this.content.filter(callback);
  return this;
};

/**
 * Create a new Collection with elements matching the where clause from the Base Collection
 * @see Collection.filter
 * @param   {Collection}    collection  - Base Collection
 * @param   {Object|string} key         - Key/Value pair Object for filter or key to be filtered
 * @param   {*}             [value]     - value of key
 * @return {Collection}                 - a new Collection
 */
Collection.where = (collection, key, value) => {
  if (value) {
    return new Collection(collection.content.filter((element) => element[key] === value));
  }

  return new Collection(collection.content.filter((element) => !Object.entries(key)
    .map(([k, v]) => element[k] === v)
    .includes(false),
  ));
};

/**
 * Keep a subset of this collection matching the where clause
 * @see Collection#filter
 * @param   {Object|string} key     - Key/Value pair Object for filter or key to be filtered
 * @param   {*}             [value] - value of key
 * @return {Collection}             - this collection
 */
Collection.prototype.where = function (key, value) {
  if (value) {
    this.content = this.content.filter((element) => element[key] === value);
    return this;
  }

  this.content = this.content.filter((element) => !Object.entries(key)
    .map(([k, v]) => element[k] === v)
    .includes(false),
  );
  return this;
};

/**
 * Create a new Collection with elements exist in the where clause from the Base Collection
 * @see Collection.where
 * @param   {Collection}    collection  - Base Collection
 * @param   {Object|string} key         - Key/Value pair Object for filter or key to be filtered
 * @param   {Array}         [value]     - possible values of key
 * @return {Collection}                 - a new Collection
 */
Collection.whereIn = (collection, key, value) => {
  if (value) {
    return new Collection(collection.content.filter((element) => value.includes(element[key])));
  }

  return new Collection(collection.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => val.includes(element[key]))
    .includes(false),
  ));
};

/**
 * Keep a subset of this collection matching values in the where clause
 * @see Collection#where
 * @param   {Object|string} key     - Key/Value pair Object for filter or key to be filtered
 * @param   {Array}         [value] - possible values of key
 * @return {Collection}             - this collection
 */
Collection.prototype.whereIn = function (key, value) {
  if (value) {
    this.content = this.content.filter((element) => value.includes(element[key]));
    return this;
  }

  this.content = this.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => val.includes(element[key]))
    .includes(false),
  );
  return this;
};

/**
 * Create a new Collection with numbers exist within the range of the where clause from the Base Collection
 * @see Collection.where
 * @param   {Collection}      collection  - Base Collection
 * @param   {Object|string}   key         - Key/Value pair Object for filter or key to be filtered
 * @param   {number|number[]} [min]       - minimum value if number, or array of numbers
 * @param   {number}          [max]       - maximum value
 * @return {Collection}                   - a new Collection
 */
Collection.whereBetween = (collection, key, min, max) => {
  if (max) {
    return new Collection(collection.content.filter((element) => element[key] >= min && element[key] <= max));
  }

  if (min) {
    return new Collection(collection.content.filter((element) =>
      element[key] >= Math.min(min) && element[key] <= Math.max(min)));
  }

  return new Collection(collection.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => element[key] >= Math.min(val) && element[key] <= Math.max(val))
    .includes(false),
  ));
};

/**
 * Keep a subset of this collection with numbers within the range of the where clause
 * @see Collection.where
 * @param   {Object|string}   key         - Key/Value pair Object for filter or key to be filtered
 * @param   {number|number[]} [min]       - minimum value if number, or array of numbers
 * @param   {number}          [max]       - maximum value
 * @return {Collection}                   - a new Collection
 */
Collection.prototype.whereBetween = function (key, min, max) {
  if (max) {
    this.content = this.content.filter((element) => element[key] >= min && element[key] <= max);
    return this;
  }

  if (min) {
    this.content = this.content.filter((element) =>
      element[key] >= Math.min(min) && element[key] <= Math.max(min));
    return this;
  }

  this.content = this.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => element[key] >= Math.min(val) && element[key] <= Math.max(val))
    .includes(false),
  );
  return this;
};

/**
 * Create a new Collection with elements not matching the where clause from the Base Collection
 * @see Collection.filter
 * @param   {Collection}    collection  - Base Collection
 * @param   {Object|string} key         - Key/Value pair Object for filter or key to be filtered
 * @param   {*}             [value]     - value of key
 * @return {Collection}                 - a new Collection
 */

Collection.whereNot = (collection, key, value) => {

  if (value) {
    return new Collection(collection.content.filter((element) => element[key] !== value));
  }

  return new Collection(collection.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => element[key] !== val)
    .includes(false),
  ));
};

/**
 * Keep a subset of this collection not matching the where clause
 * @see Collection#filter
 * @param   {Object|string} key     - Key/Value pair Object for filter or key to be filtered
 * @param   {*}             [value] - value of key
 * @return {Collection}             - this collection
 */
Collection.prototype.whereNot = function (key, value) {
  if (value) {
    this.content = this.content.filter((element) => element[key] !== value);
    return this;
  }

  this.content = this.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => element[key] !== val)
    .includes(false),
  );
  return this;
};

/**
 * Create a new Collection with elements not exist in the where clause from the Base Collection
 * @see Collection.where
 * @param   {Collection}    collection  - Base Collection
 * @param   {Object|string} key         - Key/Value pair Object for filter or key to be filtered
 * @param   {Array}         [value]     - possible values of key
 * @return {Collection}                 - a new Collection
 */

Collection.whereNotIn = (collection, key, value) => {
  if (value) {
    return new Collection(collection.content.filter((element) => !value.includes(element[key])));
  }

  return new Collection(collection.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => !val.includes(element[key]))
    .includes(false),
  ));
};

/**
 * Keep a subset of this collection not matching values in the where clause
 * @see Collection#where
 * @param   {Object|string} key     - Key/Value pair Object for filter or key to be filtered
 * @param   {Array}         [value] - possible values of key
 * @return {Collection}             - this collection
 */
Collection.prototype.whereNotIn = function (key, value) {
  if (value) {
    this.content = this.content.filter((element) => !value.includes(element[key]));
    return this;
  }

  this.content = this.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => !val.includes(element[key]))
    .includes(false),
  );
  return this;
};

/**
 * Create a new Collection with numbers not exist within the range of the where clause from the Base Collection
 * @see Collection.where
 * @param   {Collection}      collection  - Base Collection
 * @param   {Object|string}   key         - Key/Value pair Object for filter or key to be filtered
 * @param   {number|number[]} [min]       - minimum value if number, or array of numbers
 * @param   {number}          [max]       - maximum value
 * @return {Collection}                   - a new Collection
 */

Collection.whereNotBetween = (collection, key, min, max) => {
  if (max) {
    return new Collection(collection.content.filter((element) => element[key] < min || element[key] > max));
  }

  if (min) {
    return new Collection(collection.content.filter((element) =>
      element[key] < Math.min(min) || element[key] > Math.max(min)));
  }

  return new Collection(collection.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => element[key] < Math.min(val) || element[key] > Math.max(val))
    .includes(false),
  ));
};

/**
 * Keep a subset of this collection matching numbers not within the range of the where clause
 * @see Collection.where
 * @param   {Object|string}   key         - Key/Value pair Object for filter or key to be filtered
 * @param   {number|number[]} [min]       - minimum value if number, or array of numbers
 * @param   {number}          [max]       - maximum value
 * @return {Collection}                   - this collection
 */
Collection.prototype.whereNotBetween = function (key, min, max) {
  if (max) {
    this.content = this.content.filter((element) => element[key] < min || element[key] > max);
    return this;
  }

  if (min) {
    this.content = this.content.filter((element) =>
      element[key] < Math.min(min) || element[key] > Math.max(min));
    return this;
  }

  this.content = this.content.filter((element) => !Object.entries(key)
    .map(([key, val]) => element[key] < Math.min(val) || element[key] > Math.max(val))
    .includes(false),
  );
  return this;
};

/**
 * Create a new Collection with duplicate values/entries removed from Base Collection
 * @param   {Collection}      collection  - Base Collection
 * @param   {Function|string} [key]       - the key for unique values
 * @return {Collection}                   - a new Collection
 */
Collection.unique = (collection, key) => {
  if (key && key.constructor.name === 'Function') {
    const values = collection.content.map((element) => key(element));
    const uniques = [...new Set(values)];
    return new Collection(collection.content.filter((element, index) => index === uniques.indexOf(values)));
  }

  if (key) {
    const uniques = [...new Set(collection.content.map((element) => element[key]))];
    return new Collection(collection.content.filter((element, index) => index === uniques.indexOf(element[key])));
  }

  return new Collection([...new Set(collection.content)]);
};

/**
 * Keep a subset of this collection with duplicate values/entries removed
 * @param   {Function|string} [key]       - the key for unique values
 * @return {Collection}                   - this collection
 */
Collection.prototype.unique = (key) => {
  if (key && key.constructor.name === 'Function') {
    const values = this.content.map((element) => key(element));
    const uniques = [...new Set(values)];
    this.content = this.content.filter((element, index) => index === uniques.indexOf(values));
    return this;
  }

  if (key) {
    const uniques = [...new Set(this.content.map((element) => element[key]))];
    this.content = this.content.filter((element, index) => index === uniques.indexOf(element[key]));
    return this;
  }

  this.content = [...new Set(this.content)];
  return this;
};


module.exports = Collection;
